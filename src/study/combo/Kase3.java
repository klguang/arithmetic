package study.combo;

import java.util.Stack;

public class Kase3 {
    static int cnt = 0;
    static Stack<Integer> s = new Stack<Integer>();
 
    /**
     * 递归方法，当前已抽取的小球个数与要求抽取小球个数相同时，退出递归
     * @param curnum - 当前已经抓取的小球数目
     * @param curmaxv - 当前已经抓取小球中最大的编号
     * @param maxnum - 需要抓取小球的数目
     * @param maxv - 待抓取小球中最大的编号
     */
    public static void kase3(int curnum, int curmaxv,  int maxnum, int maxv){
        if(curnum == maxnum){
            cnt++;
            System.out.println(s);
            return;
        }
 
        for(int i = curmaxv + 1; i <= maxv; i++){ // i <= maxv - maxnum + curnum + 1
            s.push(i);
            kase3(curnum + 1, i, maxnum, maxv);
            s.pop();
        }
    }
 
    public static void main(String[] args){
        kase3(0, 0, 4, 8);
        System.out.println(cnt);
    }
}
