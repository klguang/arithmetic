package study.combo;
import java.util.Stack;

public class Kase2 {
    static int cnt = 0;
    static Stack<Integer> s = new Stack<Integer>();
    static boolean[] used = new boolean[10000];
 
    /**
     * 递归方法，当实际选取的小球数目与要求选取的小球数目相同时，跳出递归
     * @param minv - 小球编号的最小值
     * @param maxv - 小球编号的最大值
     * @param curnum - 当前已经确定的小球的个数
     * @param maxnum - 要选取的小球的数目
     */
    public static void kase2(int minv,int maxv,int curnum, int maxnum){
        if(curnum == maxnum){
            cnt++;
            System.out.println(s);
            return;
        }
 
        for(int i = minv; i <= maxv; i++){
            if(!used[i]){
                s.push(i);
                used[i] = true;
                kase2(minv, maxv, curnum+1, maxnum);
                s.pop();
                used[i] = false;
            }
        }
    }
 
    public static void main(String[] args){
        kase2(1, 5, 0, 3);
        System.out.println(cnt);
    }
}
