package study.combo;

import java.util.ArrayList;
import java.util.List;

import util.MyPrinter2;

public class Permutations {

	private static void arrangementAll(String[] dataList, String[] resultList, int resultIndex, List<String[]> result) {
		if (resultIndex >= resultList.length) {
			String[] r = new String[resultList.length];
			System.arraycopy(resultList, 0, r, 0, resultList.length);
			result.add(r);
			return;
		}

		for (int i = 0; i < dataList.length; i++) {
			resultList[resultIndex] = dataList[i];
			arrangementAll(dataList, resultList, resultIndex + 1, result);
		}
	}

	/**
	 * 全排列，个数
	 * 
	 * @param dataList
	 * @param n
	 * @return 结果列表
	 */
	public static List<String[]> arrangementAll(String[] dataList, int n) {
		List<String[]> result = new ArrayList<>();
		arrangementAll(dataList, new String[n], 0, result);
		return result;

	}

	public static void main(String[] args) {
		List<String[]> result =arrangementAll(new String[] { "1", "2", "3" ,"4"}, 2);

		MyPrinter2.print(result);

	}

}
